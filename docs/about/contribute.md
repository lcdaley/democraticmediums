# Contribute

Democratic Mediums is a participatory effort that depends on community contributions. Please share your expertise by helping to make this resource more useful for others.

## How to contribute

To add or edit content, fork [the project on GitLab](https://gitlab.com/medlabboulder/democraticmediums) and submit a merge request once you have made your changes. Follow these instructions to take advantage of the website's integration with the GitLab online editor:

* Navigate to the page you want to edit on the Democratic Mediums website and click the edit button (a small pencil icon)
* Sign into GitLab with an existing account or create a new one
* GitLab will offer to create a "fork" for you, which is a copy of the site in your GitLab account—do so!
* Edit the page as you wish; to edit multiple pages, it may be easier to use GitLab's "Web IDE"
* Add a "commit message" summarizing your suggested change and click "Commit changes"
* Fill out any additional information you want in the merge request form and submit it
* Your merge will be approved, if appropriate, as soon as possible

The website is built with [MkDocs](https://www.mkdocs.org/), a simple but powerful static site generator for Markdown files. All site content is located in the [docs/ directory](https://gitlab.com/medlabboulder/democraticmediums/tree/master/docs), which in turn is organized roughly as it appears on the site itself.

Please send any questions to [medlab@colorado.edu](mailto:medlab@colorado.edu). If you prefer not to use GitLab, you can also simply email your suggestions there.

## Contribution guidelines

### Standards for inclusion

The "mediums" included in this directory should aim to capture the most discrete, modular elements of democratic practice. That is, *Westminster system* would not be a good entry because it is a historical phenomenon that combines many elements. Some of those elements, such as *proportional representation* and *parliamentarianism*, however, would be useful to include.

### New entries

To create a new entry, copy the content of the [docs/mediums/.template.md](https://gitlab.com/medlabboulder/democraticmediums/blob/master/docs/mediums/.template.md) file into a new file at docs/mediums/NAME.md. Follow the format of the template, and look to existing entries for examples.

### Markup

Democratic Mediums files are written in [Markdown](https://commonmark.org/help/), a simple markup language that translates to HTML. (When necessary, HTML may be used as well.) For information about how the MkDocs system handles links, metadata, and other content, see [its documentation](https://www.mkdocs.org/user-guide/writing-your-docs/).

### Style

Democratic Mediums is not meant to be exhaustive or encyclopedic. Entries should strive above all for brevity, clarity, and practicality. The model for an entry should be not a Wikipedia article but a technical manual, like [API documentation](https://www.mulesoft.com/resources/api/guidelines-api-documentation). Concise, grammatical bullet points and examples are more appropriate than lengthy prose discussions. The purpose is not completeness (leave that to sources in the Further Resources section) but usability for governance designers.

Language should be as simple and direct, avoiding specialized terminology wherever possible.

### References

Most citations can be made through links to source material and in-text indications of the source. External sources that are especially useful can be included in the Further Resources section of an entry.

Bibliographic references can be made in the [Chicago Manual of Style bibliography-entry format](https://www.chicagomanualofstyle.org/tools_citationguide/citation-guide-1.html) or a format that similarly enables alphabetization. Include links in bibliographic entries where possible.

## Roadmap

The following are future goals for this project:

* Add YAML front matter to all pages for cross-compatibility (e.g., with Jekyll, should we require tagging support)
* Populate more of the entries in the directory
* Build community and a robust contributor base
* Commission uniform illustrated diagrams for each entry
* Present the mediums as less isolated and more modular, enabling users to assemble them into packages and processes
* Gamification features enabling users to test mediums against one another

To get involved in developing these strategic objectives, please write to [medlab@colorado.edu](mailto:medlab@colorado.edu).
