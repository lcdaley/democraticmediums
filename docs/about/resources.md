# Resources

Help build a bibliography in the [Democratic Mediums group on Zotero](https://www.zotero.org/groups/2315526/democratic_mediums).

## Related directories

* [#dgov foundation](https://dgov.foundation)
    - [Wiki](https://wiki.dgov.foundation/)
    - [DAO Landscape Report](https://dao-landscape.gitbook.io/project/), including "On-Chain Governance Case Studies"
* [Liberating Structures](http://www.liberatingstructures.com/)
* [Web 3 Revenue Primitives](https://github.com/FEMBusinessModelsRing/web3_revenue_primitives/)
* Wikipedia:
    - [Comparison of electoral systems](https://en.wikipedia.org/wiki/Comparison_of_electoral_systems)
    - [Portal: Elections](https://en.wikipedia.org/wiki/Portal:Elections)
* [Wise Democracy Project patterns](https://www.wd-pl.com/patterns-by-name/)
* yunity, "[Modules for sustainable organization](https://yunity.atlassian.net/wiki/spaces/YUN/pages/18317679/Modules+for+sustainable+organization)"