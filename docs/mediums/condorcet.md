# Condorcet

Condorcet is an election method that selects the candidate, if such a candidate exists, that would win a [majority vote](majority_voting.md) against all other candidates. Ballots may take the form of either a single [ranked choice](ranked_choice.md) vote or a sequence of runoffs.

There is not always a "Condorcet winner," and various implementations provide differing methods of determining a winner in such cases.

**Input:** preferential vote or runoffs

**Output:** Condorcet winner or circular paradox

## Background

The Condorcet method takes its name from an early promoter, the 18th-century French mathematician Marie Jean Antoine Nicolas Caritat, who was the Marquis de Condorcet. The method itself was [first described by Ramon Llull](https://www.math.uni-augsburg.de/htdocs/emeriti/pukelsheim/2001a.html) in 1299. A version of it is used in Robert's Rules of Order, first published in 1876.

Recently, it has attracted the interest of software developers and has been adopted by several prominent Free Software communities and Pirate Party groups.


## Feedback loops

### Sensitivities

* Prevents some potential for gaming that is possible in other voting methods

### Oversights

* The complexity of the system may lead to confusion

## Implementations

### Communities

* Free Software communities
    - [Debian Project](https://www.debian.org/vote/)
    - [Python Software Foundation voting process](https://www.python.org/dev/peps/pep-8001/)
    - Wikimedia Foundation
* Pirate Party of Sweden uses it for primary elections

### Tools

* [Condorcet Internet Voting Service](https://civs.cs.cornell.edu/) at Cornell University

## Further resources

* "[Condorcet method](https://en.wikipedia.org/wiki/Condorcet_method)" at Wikipedia
* Schulze, Markus. "[A new monotonic, clone-independent, reversal symmetric, and condorcet-consistent single-winner election method](https://link.springer.com/article/10.1007/s00355-010-0475-4)." _Social Choice and Welfare_ 36, no. 2 (February 2011).
