# Democratic Mediums

**Patterns for decision, deliberation, and noise**

*A project of the [Media Enterprise Design Lab](http://cmci.colorado.edu/medlab/)
at the University of Colorado Boulder.*

Democratic Mediums is a work in progress. View the live demo at [medlabboulder.gitlab.io/democraticmediums](https://medlabboulder.gitlab.io/democraticmediums/).

## Contribute

Visit the [Contribute page](https://medlabboulder.gitlab.io/democraticmediums/about/contribute/) for information about how to help build Democratic Mediums.
